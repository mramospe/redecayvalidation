'''
Options file to create a n-tuple given the name of the decay.
'''
# Python
import os
# Gaudi
from Configurables import CombineParticles, DaVinci, DecayTreeTuple, MCDecayTreeTuple, LoKi__Hybrid__TupleTool, FilterDesktop
from StandardParticles import StdAllNoPIDsMuons
from DecayTreeTuple.Configuration import *
from PhysConf.Selections import Selection, SelectionSequence

envvar = 'RDVDECAY'

decay = os.environ.get(envvar, None)

if decay is None:
    raise RuntimeError(
        'Must specify the type of decay setting the environmental variable "{}"'.format(envvar))


def matched_muons():
    '''
    Create a selection of mc-matched muons
    '''
    algorithm = FilterDesktop('MatchMuons',
                              Code="mcMatch('mu+') | mcMatch('mu-')",
                              Preambulo=['from LoKiPhysMC.decorators import *', 'from LoKiPhysMC.functions import mcMatch'])
    selection = Selection('SelMatchMuons', Algorithm=algorithm,
                          RequiredSelections=[StdAllNoPIDsMuons])
    sequence = SelectionSequence('SeqMatchMuons', TopSelection=selection)
    return sequence


if decay == 'K0S2mu2':

    head = 'KS0'

    decay_descriptor = 'KS0 -> mu+ mu-'

    mcdescriptor = 'KS0 => ^mu+ ^mu-'
    descriptor = 'KS0 -> ^mu+ ^mu-'

    branches = {
        'KS0': '^(KS0 -> mu+ mu-)',
        'mup': 'KS0 -> ^mu+ mu-',
        'mum': 'KS0 -> mu+ ^mu-',
    }
    mcbranches = {
        'KS0': '^(KS0 => mu+ mu-)',
        'mup': 'KS0 => ^mu+ mu-',
        'mum': 'KS0 => mu+ ^mu-',
    }

    particles = [matched_muons()]

else:
    raise ValueError('Unknown decay "{}"'.format(decay))

# Configure MC-truth tuple
mctuple = MCDecayTreeTuple('mctruth')
mctuple.Decay = mcdescriptor
mctuple.addBranches(mcbranches)
mctuple.ToolList = [
    'MCTupleToolKinematic',
    'MCTupleToolPrompt',
    'MCTupleToolHierarchy',
    'MCTupleToolPID',
    'MCTupleToolReconstructed',
]
# Configure MC-match tuple
ntuple = DecayTreeTuple('mcmatch')
ntuple.Decay = descriptor
ntuple.addBranches(branches)
ntuple.ToolList = [
    'TupleToolKinematic',
    'TupleToolPid',
    'TupleToolANNPID',
    'TupleToolGeometry',
    'TupleToolAngles',
    'TupleToolEventInfo',
    'TupleToolMuonPid',
    'TupleToolTrackInfo',
    'TupleToolRecoStats',
]
head_tool = getattr(ntuple, head).addTupleTool(
    'LoKi::Hybrid::TupleTool/HeadVars')
head_tool.Variables = {
    'DOCA': "PFUNA(AMAXDOCA(''))",
    'DOCACHI2': "DOCACHI2MAX",
    'ADMASS': "ADMASS('KS0')",
    'ETA': "ETA",
}

for b in filter(lambda s: s != head, ntuple.Branches):
    getattr(ntuple, b).addTupleTool(
        'LoKi::Hybrid::TupleTool/{}Vars'.format(b)).Variables = {'ETA': "ETA"}

particles_sequences = [p.sequence() for p in particles]
particles_selections = [p.selection() for p in particles]
particles_locations = [p.outputLocation() for p in particles]

combiner = CombineParticles('Combiner')
combiner.DecayDescriptor = decay_descriptor
combiner.MotherCut = "mcMatch('{}')".format(
    decay_descriptor.replace('->', '=>'))
combiner.Preambulo = ['from LoKiPhysMC.decorators import *',
                      'from LoKiPhysMC.functions import mcMatch']
selection = Selection('Selection', Algorithm=combiner,
                      RequiredSelections=particles_selections)
sequence = SelectionSequence('Sequence', TopSelection=selection)

ntuple.Inputs = [sequence.outputLocation()]

# Add the algorithms to DaVinci
DaVinci().UserAlgorithms += [mctuple] + \
    particles_sequences + [sequence.sequence(), ntuple]

# Define the name of the ROOT files
DaVinci().TupleFile = '{}.root'.format(decay)
DaVinci().HistogramFile = '{}_DVHistos.root'.format(decay)

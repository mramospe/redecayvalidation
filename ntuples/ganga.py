#!/usr/bin/env ganga
'''
Send jobs to create n-tuples.
'''
import argparse
import os
import shutil
import tempfile


def main(name, options, decay, data, backend, files_per_job, test):
    '''
    Main function to execute.
    '''
    backend = Dirac() if backend == 'dirac' else Local()

    splitter = SplitByFiles(filesPerJob=files_per_job,
                            maxFiles=-1, ignoremissing=False)

    # Define the application
    path_to_davinci = os.path.join(
        os.environ['HOME'], 'cmtuser/DaVinciDev_v50r4')
    if os.path.isdir(os.path.expanduser(path_to_davinci)):
        application = GaudiExec()
        application.directory = path_to_davinci
    else:
        application = prepareGaudiExec('DaVinci', 'v50r4')

    tmpdir = tempfile.mkdtemp()

    decay_file = os.path.join(tmpdir, 'decay.py')
    with open(decay_file, 'wt') as f:
        f.write('import os; os.environ["RDVDECAY"] = "{}"\n'.format(decay))
    options.insert(0, decay_file)

    application.platform = 'x86_64-slc6-gcc8-opt'
    application.options = options

    # Get the data
    with open(data) as f:
        files = list(filter(lambda l: len(l), f.read().split('\n')))
        if test:
            files = files[-1:]
        dataset = LHCbDataset(files)

    # Make the job and submit
    job = Job(
        name=name,
        application=application,
        splitter=splitter,
        outputfiles=['*.root'],
        parallel_submit=True,
        backend=backend,
        inputfiles=options,
        inputdata=dataset,
    )
    job.application.prepare()
    job.submit()

    shutil.rmtree(tmpdir)


parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('--name', '-n',
                    default='tuple-making',
                    help='Name of the job')
parser.add_argument('--options', '-o', nargs='+',
                    help='Option files to use')
parser.add_argument('--data', '-d', type=str, required=True,
                    help='Path to a file containing the LFNs to process')
parser.add_argument('--decay', '-s', type=str, required=True,
                    choices=('K0S2mu2',),
                    help='Decay to process')
parser.add_argument('--backend', '-b', choices=('local', 'dirac'),
                    default='local',
                    help='Backend for the submission of jobs')
parser.add_argument('--files-per-job', '-f', type=int,
                    default=10,
                    help='Number of files to process per job')
parser.add_argument('--test', action='store_true',
                    help='Whether to process a single file to test')
args = parser.parse_args()

main(**vars(args))

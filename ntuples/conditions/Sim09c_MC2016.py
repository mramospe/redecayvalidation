'''
Tags used in Sim09c for 2016 data type
'''
from Configurables import DaVinci

DaVinci().DataType = '2016'
DaVinci().InputType = 'LDST'
DaVinci().CondDBtag = 'sim-20170721-2-vc-md100'
DaVinci().DDDBtag = 'dddb-20170721-3'
DaVinci().Simulation = True
DaVinci().Lumi = False
DaVinci().PrintFreq = 3000
DaVinci().EvtMax = -1
DaVinci().SkipEvents = 0

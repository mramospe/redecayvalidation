# Generation of ROOT files for ReDecay validation for strange decays

The option file [make_tuple.py](make_tuple.py) has the sequence to generate both a MC-truth and MC-matched n-tuple for a decay of interest.
The decay to process is specified by seeting the environmental variable RDVDECAY.
The current allowed decays are:
 - K0S2mu2: create tuples for $`K_\text{S}^0 \rightarrow \mu^+\mu^-`$ candidates.

In order to submit the jobs with [Ganga](https://gitlab.cern.ch/ganga), the script [ganga.py](ganga.py) can be used.
To see the available input arguments type
```bash
./ganga.py -h
```
and the options will be displayed.
The minimal job would contain the decay to process, the options file and a text file containing the list of LFNs.
Under [catalogs](catalogs), different text files corresponding to simulated samples can be found.
In [conditions](conditions), options files defining the tags and data types are stored.
An example of submission of the jobs is
```bash
./ganga.py --decay K0S2mu2 --data catalogs/K0S2mu2_ReDecay.txt --options make_tuple.py conditions/Sim09c_MC2016.py
```

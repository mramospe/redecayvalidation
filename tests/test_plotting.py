'''
Tests for the "plotting" module.
'''

from ReDecayValidation import plotting


def test_format():
    '''
    Test to apply the matplotlib format.
    '''
    plotting.apply_format()
    plotting.apply_format(2.)

'''
Tests for the "core" module.
'''
import logging

from ReDecayValidation import core

logger = logging.getLogger(__name__)


def test_logger():
    '''
    Test the configuration of the logger.
    '''
    core.configure_logger()

    logger.info('This is an usual message')

    logger.warning('This is a warning')

    logger.error('This is an error')

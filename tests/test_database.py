'''
Tests for the "database" module.
'''

from ReDecayValidation import database


def test_database():
    '''
    Test the DataBase class.
    '''
    db = database.Database()

    # Singleton condition
    assert database.Database() is db

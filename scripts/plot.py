#!/usr/bin/env python
'''
Compare variables for different samples
'''
import argparse
import collections
import cycler
import hep_rfm
import logging
import matplotlib.pyplot as plt
import numpy as np
import re
import uproot

from ReDecayValidation import core, database, plotting


logger = logging.getLogger(__name__)

Variable = collections.namedtuple(
    'Variable', ['name', 'title', 'bins', 'min', 'max'])


def plot(samples, variables, title, tree_path, selection=None):
    '''
    Plot variables from samples on a figure with the given title.
    '''
    logger.info('Will plot variables: [{}]'.format(
        ', '.join(v.name for v in variables)))

    fig, axs = plt.subplots(2, 2, figsize=(10, 7))

    styles = cycler.cycler(linestyle=['solid', 'dashed', 'dashdot', 'dotted'])

    table = hep_rfm.Table.read(database.table_path())

    if selection is not None:
        logger.info(f'Applying selection: "{selection}"')

    for var, ax in zip(variables, axs.flatten()):

        for smp, sty in zip(samples, styles()):

            smp_info = database.mc_sample(smp, table=table)

            with uproot.open(smp_info.path) as ifile:

                arr = ifile[tree_path][var.name].array()

                if selection is not None:

                    tree = ifile[tree_path]

                    expr = (
                        b'(' + b')|('.join(reversed(sorted(tree.keys()))) + b')').decode()

                    branches = set(it.group()
                                   for it in re.finditer(expr, selection))

                    chunks = [df.eval(selection) for df in tree.pandas.iterate(
                        branches, entrysteps=10000)]

                    arr = arr[np.concatenate(chunks)]

            ax.hist(arr, bins=var.bins, range=(var.min, var.max),
                    linestyle=sty['linestyle'], label=smp_info.sim_type, histtype='step')

        ax.set_xlim(var.min, var.max)
        ax.set_xlabel(var.title)
        ax.set_ylabel('Candidates')
        ax.legend(fontsize=10)

    fig.suptitle(title, fontsize=18)
    fig.tight_layout(rect=[0, 0, 1, 0.95])


def main(samples, show, match_selection, truth_selection):
    '''
    Main function to execute.
    '''
    logger.info('Working with: {}'.format(samples))

    # Compare the MC-truth samples
    variables = (
        Variable(
            'KS0_TRUEPT', f'\({plotting.KS0}~\\text{{true}}~p_T~{plotting.momentum}\)', 100, 0, 3e3),
        Variable(
            'KS0_TRUEP_Z', f'\({plotting.KS0}~\\text{{true}}~p_z~{plotting.momentum}\)', 100, 0, 4e4),
        Variable(
            'KS0_TRUETAU', f'\({plotting.KS0}~\\text{{true}}~c\\tau~{plotting.distance}\)', 100, 0, 0.3),
        Variable('KS0_TRUEENDVERTEX_Z',
                 f'\({plotting.KS0}~\\text{{true}}~\\text{{SV}}_z~{plotting.distance}\)', 100, -100, 650),
    )

    logger.info('Process MC-truth')

    plot(samples, variables, title='MC-truth',
         tree_path='mctruth/MCDecayTree', selection=truth_selection)

    # Compare the MC-matched samples
    variables = (
        Variable(
            'KS0_PT', f'\({plotting.KS0}~p_T~{plotting.momentum}\)', 100, 0, 3e3),
        Variable(
            'KS0_P', f'\({plotting.KS0}~p~{plotting.momentum}\)', 100, 0, 8e4),
        Variable(
            'KS0_DOCA', f'\({plotting.KS0}~\\text{{DOCA}}~{plotting.distance}\)', 100, 0, 0.8),
        Variable('KS0_IP_OWNPV',
                 f'\({plotting.KS0}~\\text{{IP}}~{plotting.distance}\)', 100, 0, 1),
    )

    logger.info('Process MC-match')

    plot(samples, variables, title='MC-match',
         tree_path='mcmatch/DecayTree', selection=match_selection)

    # Show the plots if asked to do so
    if show:
        plt.show()


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('samples', nargs='+',
                        help='Samples to plot')
    parser.add_argument('--show', '-s', action='store_true',
                        help='Whether to show the plots or not')
    parser.add_argument('--truth-selection', '-t', type=str, default=None,
                        help='Selection to be applied to the MC-truth samples')
    parser.add_argument('--match-selection', '-m', type=str, default=None,
                        help='Selection to be applied to the MC-match samples')
    args = parser.parse_args()

    core.configure_logger()
    plotting.apply_format()

    main(**vars(args))

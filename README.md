# Repository to validata ReDecay for strange decays

This repository contains the scripts to create n-tuples and do validation studies for the use of ReDecay with strange decays at LHCb.
The directory [ntuples](ntuples) contains the option files and data catalogs to run with [DaVinci](https://gitlab.cern.ch/lhcb/DaVinci) and create the n-tuples.
Under [scripts](scripts), the different validation scripts to create plots and do comparisons are stored.

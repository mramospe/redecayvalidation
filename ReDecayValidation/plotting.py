'''
Tools to make plots and format matplotlib figures
'''
import matplotlib.pyplot as plt


def apply_format(scale=1.):
    '''
    Apply the LHCb-style
    '''
    plt.rc('font', family='serif', size=15 * scale)
    plt.rc('text', usetex=True)
    plt.rcParams['text.latex.preamble'] = r'\usepackage{amsmath}'
    plt.rcParams['lines.linewidth'] = 2
    plt.rcParams['axes.linewidth'] = 1.5
    plt.rcParams['xtick.major.width'] = 1
    plt.rcParams['ytick.major.width'] = 1
    plt.rcParams['xtick.minor.width'] = 1
    plt.rcParams['ytick.minor.width'] = 1
    plt.rcParams['xtick.major.size'] = 10
    plt.rcParams['ytick.major.size'] = 10
    plt.rcParams['xtick.minor.size'] = 5
    plt.rcParams['ytick.minor.size'] = 5
    plt.rcParams['legend.frameon'] = True
    plt.rcParams['legend.fontsize'] = plt.rcParams['font.size'] * 0.6


# Particles
KS0 = r'K_\text{S}^0'
mu = r'\mu'
mup = r'\mu^+'
mum = r'\mu^-'
mumu = mup + mum

# Units
mass = r'(\text{MeV}/c^2)'
momentum = r'(\text{MeV}/c)'
distance = r'(\text{mm})'

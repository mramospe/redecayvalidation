'''
Module to access the data
'''
import argparse
import hep_rfm
import logging
import collections
import socket
import tempfile

# Local
from ReDecayValidation import core


class Database(object):

    __instance = None

    def __new__(cls):
        '''
        Singleton constructor.
        '''
        if Database.__instance is not None:
            return Database.__instance

        mgr = hep_rfm.Manager()
        mgr.add_table(
            '@mastercr1.igfae.usc.es:/RD01/ReDecayValidation/table.db', 'ssh')
        mgr.add_table(
            'root://eoslhcb.cern.ch//eos/lhcb/wg/RD/RnS/ReDecayValidation/table.db', 'xrootd')

        Database.__instance = object.__new__(cls)
        Database.__instance.__mgr = mgr

        return Database.__instance

    @property
    def mgr(self):
        '''
        Return the manager of the tables
        '''
        return self.__mgr


def table_path():
    '''
    Return the path to the table given the current host name.
    '''
    modifiers = {}
    host = socket.gethostname()
    if host.startswith('lxplus'):
        modifiers['xrootd_servers'] = ['eoslhcb.cern.ch']
    elif host.startswith('nodo') or host.startswith('mastercr1'):
        modifiers['ssh_hosts'] = ['mastercr1.igfae.usc.es']
    return Database().mgr.available_table(modifiers=modifiers)


# Definition of a MC sample
MCSample = collections.namedtuple('MCSample', [
                                  'name', 'path', 'decay', 'event_type', 'sim_version', 'mc_year', 'sim_type', 'polarity'])


def mc_sample(smp, table=None):
    '''
    Build an object storing the information about a simulated sample.
    New simulated sample names must follow this convention.
    '''
    table = table if table is not None else hep_rfm.Table.read(table_path())

    path = hep_rfm.available_working_path(table[smp].protocol_path)

    fields = smp.split('_')

    return MCSample(smp, path, *fields)


if __name__ == '__main__':

    def update_database(usc_user, in_nodos, nproc, tmpdir):
        '''
        Update the database
        '''
        print('--------------------------------------------')
        print('-- Synchronizing remote and local samples --')
        print('--------------------------------------------')

        ss = {'ssh_usernames': {'mastercr1.igfae.usc.es': usc_user}}
        if in_nodos:
            ss['ssh_hosts'] = ['mastercr1.igfae.usc.es']

        with tempfile.TemporaryDirectory(dir=tmpdir) as d:
            logging.getLogger(__name__).info(
                'Auxiliar directory set to "{}"'.format(d))
            Database().mgr.update(parallelize=nproc, wdir=d, modifiers=ss)

    def show_samples():
        '''
        Show the samples in the database
        '''
        print('Samples in database:')
        table = hep_rfm.Table.read(table_path())
        for k in table:
            print(k)

    core.configure_logger()

    parser = argparse.ArgumentParser(description='Update samples')

    subparsers = parser.add_subparsers(description='Mode to run')

    parser_u = subparsers.add_parser('update')
    parser_u.set_defaults(function=update_database)
    parser_u.add_argument('--usc-user', type=str, required=True,
                          help='User-name for the machines at Santiago')
    parser_u.add_argument('--in-nodos', action='store_true',
                          help='Whether we are running in the machines at Santiago or not')
    parser_u.add_argument('--nproc', type=int, default=4,
                          help='Number of processes to create')
    parser_u.add_argument('--tmpdir', type=str, default='/tmp',
                          help='Place to create temporary directories')

    parser_s = subparsers.add_parser('display')
    parser_s.set_defaults(function=show_samples)

    args = parser.parse_args()

    options = vars(args)

    function = options.pop('function')

    function(**options)

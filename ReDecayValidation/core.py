'''
Core functions and classes.
'''
import logging

__logging_format = '%(asctime)s - %(levelname)s: %(message)s'


def configure_logger():
    '''
    Configure the logger instances.
    '''
    f = logging.Formatter(__logging_format)
    h = logging.StreamHandler()
    h.setFormatter(f)
    h.setLevel(logging.INFO)
    logging.getLogger().addHandler(h)
    logging.getLogger().setLevel(logging.INFO)
